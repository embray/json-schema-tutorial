import argparse
import functools
import jsonschema


class SchemaType:
    """Checks a command-line argument against a JSON Schema."""

    type_converters = {
        'string': str,
        'integer': int,
        'number': float,
        'boolean': bool,
        'null': None
    }

    def __init__(self, schema):
        self.schema = schema
        types = schema.get('type', ['string'])
        if not isinstance(types, list):
            types = [types]
        self.types = types

    def __call__(self, val):
        # First perform type conversion
        for typ in self.types:
            conv = self.type_converters[typ]

            try:
                val = conv(val)
            except Exception:
                continue
            else:
                break
        else:
            raise argparse.ArgumentTypeError(
                f'invalid {typ} value: {val!r}')

        try:
            jsonschema.validate(val, self.schema)
        except jsonschema.ValidationError as exc:
            raise argparse.ArgumentTypeError(
                f'invalid value {val!r}: {exc.message}')

        return val


def schema_to_cli(schema, **kwargs):
    """
    Reads the given JSON Schema and generates an `argparse.ArgumentParser`.

    For any properties in the schema that have special ``"cliArg"`` keyword, an
    argument for setting that property is added to the command-line interface.

    For example, given::

        {
            'properties': {
                'foo': {
                    'type': 'integer',
                    'cliArg': '--foo'
                }
            }
        }

    The generated command-line arguments include a ``--foo`` argument which
    takes a value for the ``foo`` property, and which requires it to be an
    `int`.

    Additional ``**kwargs`` are passed to the constructor for
    `argparse.ArgumentParser`.
    """

    parser = argparse.ArgumentParser(**kwargs)
    properties = schema.get('properties', {})
    for prop_name, prop_schema in properties.items():
        if 'cliArg' not in prop_schema:
            continue

        arg_name = prop_schema['cliArg']
        prop_schema = properties[prop_name]
        prop_type = prop_schema.get('type')
        default = prop_schema.get('default')

        arg_kwargs = {
            'default': default
        }

        descr = prop_schema.get('description', '')

        # Add to the help text some type/default information:
        help_details = []
        if prop_type:
            help_details.append(f'type: {prop_type}')
        if default:
            help_details.append(f'default: {default}')

        if descr and help_details:
            arg_kwargs['help'] = f'{descr} ({", ".join(help_details)})'.lstrip()

        # Handle boolean flags; if the 'type' is 'boolean' and has a 'default'
        # of False, it is a 'store_true' flag, and vice-versa if 'default' is
        # True.  By default it's assumed 'store_true' otherwise.
        # The reason the senses are inverted is that if --some-flag is passed
        # it means we want its value to be True, as opposed to the default of
        # False.
        if prop_type == 'boolean':
            if default is False or default is None:
                action = 'store_true'
            else:
                action = 'store_false'
            arg_kwargs['action'] = action
        else:
            arg_kwargs['type'] = SchemaType(prop_schema)


        parser.add_argument(arg_name, **arg_kwargs)

    return parser

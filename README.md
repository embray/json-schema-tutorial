# 2021-07-01 Hierarchical config formats and JSON Schema

All presentation slides and notes can be found in the [Jupyter
Notebook](https://jupyter.org/) [json-schema.ipynb](blob/master/json-schema.ipynb).

If you have Jupyter Notebook installed, you can clone this repository like:

    $ git clone git@gitlri.lri.fr:SADL/2021-07-01-json-schema-tutorial.git

and run:

    $ jupyter notebook json-schema.ipynb

To run all the examples, the following packages are required:

    $ pip install pyyaml jsonschema

Alternatively, you can run the notebook on the web through
[mybinder.org](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fembray%2Fjson-schema-tutorial/HEAD?filepath=json-schema.ipynb).
